<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\TypesOfUser;
class TypesOfUsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(TypesOfUser $typesOfUser)
	{
		$arrays = [
			[
				'title' => 'admin'
			],
			[
				'title' => 'físico'
			],
			[
				'title' => 'jurídico'
			]
		];
		foreach ( $arrays as $array){
			$typesOfUser->firstOrCreate([
				'title' => $array['title']
			]);
		}

	}
}
