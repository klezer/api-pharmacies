<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(TypesOfUsersTableSeeder::class);
         $this->call(UserTableSeeder::class);
         $this->call(ContactTableSeeder::class);
         $this->call(PharmaciesTableSeeder::class);
         $this->call(StatesTableSeeder::class);
         $this->call(CityTableSeeder::class);
         $this->call(UserAddressTableSeeder::class);
         $this->call(OauthClientTableSeeder::class);
    }
}
