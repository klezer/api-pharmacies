<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\OauthClient;

class OauthClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $key_client = OauthClient::find(1);

        if($key_client)
        {
            $key_client->update(['name' => 'Dr.Pediu Pharmacies','secret' => 'JjwbYgNtXusqcs7H7oo2Xfbw9uaz76X6CXQFLb43']);
        }else{
            OauthClient::FirstOrCreate([
                'name' => 'Dr.Pediu',
                'secret' => 'JjwbYgNtXusqcs7H7oo2Xfbw9uaz76X6CXQFLb43',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0
            ]);
        }
    }
}
