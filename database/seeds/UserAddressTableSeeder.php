<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\UserAddress;

class UserAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAddress::firstOrCreate([
           'user_id'  => 1,
           'city_id' => 1,
           'number' => '111',
           'complement' => 'casa 2',
           'street_code' => '07085316',
           'street_title' => 'Rua dos Momos',
           'district_title' => 'São Paulo'
        ]);
        UserAddress::firstOrCreate([
            'user_id'  => 2,
            'city_id' => 1,
            'number' => '111',
            'complement' => 'casa 2',
            'street_code' => '07085316',
            'street_title' => 'Rua dos Momos',
            'district_title' => 'São Paulo'
        ]);
        UserAddress::firstOrCreate([
            'user_id'  => 3,
            'city_id' => 1,
            'number' => '111',
            'complement' => 'casa 2',
            'street_code' => '07085316',
            'street_title' => 'Rua dos Momos',
            'district_title' => 'São Paulo'
        ]);
    }
}
