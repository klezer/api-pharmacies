<?php

use Illuminate\Database\Seeder;

use DrPediuPharmacies\Models\City;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/cidades/cidades.json";
        $cidades_json = json_decode(file_get_contents($path), true);

        foreach ($cidades_json as $key => $cidade)
        {
            City::firstOrCreate(
                [
                    'state_id' => $cidade['Estado'],
                    'title' => $cidade['Nome']
                ]
            );
        }
    }
}
