<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\Pharmacy;

class PharmaciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Pharmacy $pharmacy)
    {
        $pharmacy->firstOrCreate([
        	'user_id' => 3,
	        'cnpj' => '05.349.305/0001-27',
	        'fantasy_name' => 'Drogarias Farmais S.a.',
	        'state_registration' => '05.422-902',
	        'responsible' => 'Roger Matias',
	        'operating_hours_of' => '06:00:00',
	        'operating_hours_until' => '20:00:00'
        ]);
    }
}
