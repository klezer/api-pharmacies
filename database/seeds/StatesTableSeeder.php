<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $states = [
                    ['title' => 'Acre',
                        'initials' => 'AC'
                    ],
                    ['title' => 'Alagoas',
                        'initials' => 'AL'
                    ],
                    ['title' => 'Amazonas',
                        'initials' => 'AM'
                    ],
                    ['title' => 'Amapá',
                        'initials' => 'AP'
                    ],
                    ['title' => 'Bahia',
                        'initials' => 'BA'
                    ],
                    ['title' => 'Ceará',
                        'initials' => 'CE'
                    ],
                    ['title' => 'Distrito Federal',
                        'initials' => 'DF'
                    ],
                    ['title' => 'Espírito Santo',
                        'initials' => 'ES'
                    ],
                    ['title' => 'Goiás',
                        'initials' => 'GO'
                    ],
                    ['title' => 'Maranhão',
                        'initials' => 'MA'
                    ],
                    ['title' => 'Minas Gerais',
                        'initials' => 'MG'
                    ],
                    ['title' => 'Mato Grosso do Sul',
                        'initials' => 'MS'
                    ],
                    ['title' => 'Mato Grosso',
                        'initials' => 'MT'
                    ],
                    ['title' => 'Pará',
                        'initials' => 'PA'
                    ],
                    ['title' => 'Paraíba',
                        'initials' => 'PB'
                    ],
                    ['title' => 'Pernambuco',
                        'initials' => 'PE'
                    ],
                    ['title' => 'Piauí',
                        'initials' => 'PI'
                    ],
                    ['title' => 'Paraná',
                        'initials' => 'PR'
                    ],
                    ['title' => 'Rio de Janeiro',
                        'initials' => 'RJ'
                    ],
                    ['title' => 'Rio Grande do Norte',
                        'initials' => 'RN'
                    ],
                    ['title' => 'Rondônia',
                        'initials' => 'RO'
                    ],
                    ['title' => 'Roraima',
                        'initials' => 'RR'
                    ],
                    ['title' => 'Rio Grande do Sul',
                        'initials' => 'RS'
                    ],
                    ['title' => 'Santa Catarina',
                        'initials' => 'SC'
                    ],
                    ['title' => 'Sergipe',
                        'initials' => 'SE'
                    ],
                    ['title' => 'São Paulo',
                        'initials' => 'SP'
                    ],
                    ['title' => 'Tocantins',
                        'initials' => 'TO'
                    ],

        ];

        foreach ($states as $key => $state){

            State::firstOrCreate(['title' => $states[$key]['title'],'initials' => $states[$key]['initials']]);
        }


    }
}
