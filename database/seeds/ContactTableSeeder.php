<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\Contact;

class ContactTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(Contact $contact)
	{
		$arrays = [
			[
				'user_id' => 1,
				'telephone_one' => '1196542351',
				'telephone_two' => '115588-0588',
				'cell_phone' => '11979852631'
			],
			[
				'user_id' => 2,
				'telephone_one' => '1196542356',
				'telephone_two' => '112588-0588',
				'cell_phone' => '1197985289'
			],
			[
				'user_id' => 3,
				'telephone_one' => '1196542322',
				'telephone_two' => '116888-0588',
				'cell_phone' => '1197985200'
			],
		];

		foreach ($arrays as $array) {

			$contact->firstOrCreate([
				'user_id' => $array['user_id'],
				'telephone_one' => $array['telephone_one'],
				'telephone_two' => $array['telephone_two'],
				'cell_phone' => $array['cell_phone']
			]);
		}
	}
}
