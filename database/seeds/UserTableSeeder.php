<?php

use Illuminate\Database\Seeder;
use DrPediuPharmacies\Models\User;

class UserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(User $user)
	{

		$arrays = [
			[
				'name' => 'admin',
				'email' => 'admin@admin.com.br',
				'password' => bcrypt('123456'),
				'cpf' => '13783252148',
				'id_types_of_users' => 1
			],
			[
				'name' => 'Fernando Ramos',
				'email' => 'fernando@farmais.com.br',
				'password' => bcrypt('123456'),
				'cpf' => '80472363182',
				'id_types_of_users' => 2
			],
			[
				'name' => 'Farmais',
				'email' => 'farmais@farmais.com.br',
				'password' => bcrypt('123456'),
				'cpf' => null,
				'id_types_of_users' => 3
			],
		];

		foreach ( $arrays as $array ) {

			$user->firstOrCreate([
				'name' => $array['name'],
				'email' => $array['email'],
				'password' => $array['password'],
				'cpf' => $array['cpf'],
				'id_types_of_users' => $array['id_types_of_users']
			]);
		}
	}
}
