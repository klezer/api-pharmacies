<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOfMedicines extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales_of_medicines', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('user_id');
			$table->integer('id_recipes_external');
			$table->unsignedBigInteger('doctor_id');
			$table->unsignedBigInteger('medicine_id');
			$table->unsignedBigInteger('patient_id');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')
				->on('users')
				->onDelete('no action')
				->onUpdate('no action');

			$table->foreign('doctor_id')->references('id')
				->on('doctors')
				->onDelete('no action')
				->onUpdate('no action');

			$table->foreign('medicine_id')->references('id')
				->on('medicines')
				->onDelete('no action')
				->onUpdate('no action');

			$table->foreign('patient_id')->references('id')
				->on('patients')
				->onDelete('no action')
				->onUpdate('no action');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sales_of_medicines');
	}
}
