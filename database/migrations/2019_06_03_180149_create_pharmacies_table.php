<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
	        $table->string('cnpj');
	        $table->string('fantasy_name')->nullable();
	        $table->string('state_registration')->nullable();
	        $table->string('responsible')->nullable();
	        $table->time('operating_hours_of')->nullable();
	        $table->time('operating_hours_until')->nullable();
            $table->timestamps();

	        $table->foreign('user_id')->references('id')
		        ->on('users')
		        ->onDelete('no action')
		        ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pharmacies');
    }
}
