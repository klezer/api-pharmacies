<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
	Route::post('api-login', 'LoginController@auth')->name('auth');
});

Route::prefix('v1')->middleware('client.passport')->group(function () {
	Route::get('list-medicines-for-cpf/{search?}', 'DrPediuMedicinesController@getRecipeForCPFUser')->name('get.recipe.cpf');
	Route::post('sales-medicines', 'DrPediuMedicinesController@updateSalesMedicines')->name('sales.medicines');
	Route::get('list-sales-medicines/{user}', 'DrPediuMedicinesController@listSalesMedicines')->name('list.sales.medicines');
});