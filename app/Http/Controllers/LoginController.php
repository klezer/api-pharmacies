<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/06/18
 * Time: 17:24
 */

namespace DrPediuPharmacies\Http\Controllers;

use Illuminate\Http\Request;
use DrPediuPharmacies\Models\User;

class LoginController extends Controller
{

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request

     * @return Response
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function auth(Request $request)
    {
        # metodo para contrução do disparo para o email ainda em construção

        $credentials = $request->only('email', 'password');
        if (auth()->guard('web')->attempt($credentials)) {
            $success = [
            	'id' => auth()->user()->id,
	            'name' => auth()->user()->name
            ];
            $user = auth()->user();
            $success['token'] =  $user->createToken('Dr.Pediu Pharmacies')->accessToken;

            return response()->json(['success' => $success], 200);
        }
        return response()->json(['status' => 'usuario não existe!']);
    }

    public function update(UpdateLoginApiRequest $updateLoginApiRequest)
    {
        $user = $this->user->find($updateLoginApiRequest->user_id);

        if($user){
            $user->update([
                'password' => $updateLoginApiRequest->password?bcrypt($updateLoginApiRequest->password):auth()->user()->password
            ]);
            return response()->json(['success' => 'Registro atualizado com sucesso!'],200);
        }
        return response()->json(['error' => 'Este usuário não existe em nossa base!'],404);
    }

}