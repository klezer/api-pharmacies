<?php

namespace DrPediuPharmacies\Http\Controllers;

use DrPediuPharmacies\Http\Requests\UpdateSalesMedicinesRequest;
use DrPediuPharmacies\Services\DrPediuServices;

class DrPediuMedicinesController extends Controller
{
	private $drPediuServices;

    public function __construct(DrPediuServices $drPediuServices)
    {
    	$this->drPediuServices = $drPediuServices;
    }

    public function getRecipeForCPFUser($cpf)
    {
    	return $this->drPediuServices->getRecipeForCPFUser($cpf);
    }
    public function updateSalesMedicines(UpdateSalesMedicinesRequest $updateSalesMedicinesRequest)
    {
    	return $this->drPediuServices->updateSalesMedicines($updateSalesMedicinesRequest);
    }
    public function listSalesMedicines($user_id)
    {
    	return $this->drPediuServices->listSalesMedicines($user_id);
    }
}
