<?php

namespace DrPediuPharmacies\Http\Requests;

use DrPediuPharmacies\Traits\FormatReturnMenssages;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSalesMedicinesRequest extends FormRequest
{
	use FormatReturnMenssages;
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'medicine_id' => 'required',
			'user_id' => 'required',
			'title' => 'required',
			'dosage' => 'required',
			'frequency' => 'required',
			'number_of_days' => 'required',
			'validity_of_medicine' => 'required',
			'note' => 'required',
			'doctor_name' => 'required',
			'doctor_cpf' => 'required',
			'doctor_crm_number' => 'required',
			'patient_name' => 'required',
			'patient_cpf' => 'required',
		];
	}
	public function messages()
	{
		return [
			'medicine_id.required' => 'O campo medicine_id é obrigatório!',
			'user_id.required' => 'O campo user_id é obrigatório!',
			'title.required' => 'O campo title é obrigatório!',
			'dosage.required' => 'O campo dosage é obrigatório!',
			'frequency.required' => 'O campo frequency é obrigatório!',
			'number_of_days.required' => 'O campo number_of_days é obrigatório!',
			'validity_of_medicine.required' => 'O campo validity_of_medicine é obrigatório!',
			'note.required' => 'O campo note é obrigatório!',
			'doctor_name.required' => 'O campo doctor_name é obrigatório!',
			'doctor_cpf.required' => 'O campo doctor_cpf é obrigatório!',
			'doctor_crm_number.required' => 'O campo doctor_crm_number é obrigatório!',
			'patient_name.required' => 'O campo patient_name é obrigatório!',
			'patient_cpf.required' => 'O campo patient_cpf é obrigatório!',
		];
	}
}
