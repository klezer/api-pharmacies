<?php


namespace DrPediuPharmacies\Services;

use Ixudra\Curl\Facades\Curl;

class CrawlerServices
{

	private $get_recipe_for_cpf;
	private $token;
	private $headers;
	private $update_sales_medicines;
	private $list_sales_medicines;

	public function __construct()
	{
		$this->get_recipe_for_cpf = env('URL_API_DR_PEDIU').'/api/v3/get-recipe-for-cpf/';
		$this->update_sales_medicines = env('URL_API_DR_PEDIU').'/api/v3/sales-medicines';
		$this->list_sales_medicines = env('URL_API_DR_PEDIU').'/api/v3/list-sales-medicines/';
		$this->token = env('URL_API_DR_PEDIU').'/oauth/token';
		$this->headers = [
			'Content-Type:	application/json',
			'Authorization: ' . $this->getToken(),
			'X-Requested-With: XMLHttpRequest'
		];
	}

	public function getRecipeForCPFUser($cpf)
	{
		return Curl::to($this->get_recipe_for_cpf.$cpf)
			->withHeaders($this->headers)
			->asJson(true)
			->get();
	}
	public function getToken()
	{
		$results = Curl::to($this->token)
			->withData( array(
				'grant_type' => env('OUTH_API_DR_PEDIU_GRANT_TYPE'),
				'client_id' => env('OUTH_API_DR_PEDIU_CLIENT_ID'),
				'client_secret' => env('OUTH_API_DR_PEDIU_CLIENT_SECRET'),
			))
			->post();

		preg_match('/"access_token":"(.*)"/',$results,$match);
		return $match[1];
	}

	public function updateSalesMedicines($request)
	{
		$results = Curl::to($this->update_sales_medicines)
			->withHeaders($this->headers)
			->withData( array(
				'medicine_id' => $request->medicine_id,
			))
			->asJson(true)
			->withResponseHeaders()
			->returnResponseObject()
			->post();
		return $results->content;
	}

	public function listSalesMedicines($id_medicine)
	{
		$results = Curl::to($this->list_sales_medicines.$id_medicine)
			->withHeaders($this->headers)
			->asJson(true)
			->withResponseHeaders()
			->returnResponseObject()
			->get();
		return $results->content;
	}
}