<?php


namespace DrPediuPharmacies\Services;

use DrPediuPharmacies\Models\Doctor;
use DrPediuPharmacies\Models\Medicine;
use DrPediuPharmacies\Models\Patient;
use DrPediuPharmacies\Models\SalesOfMedicine;
use DrPediuPharmacies\Util\Util;

class DrPediuServices
{
	private $crawlerServices;
	private $salesOfMedicine;
	private $doctor;
	private $patient;
	private $medicine;

	public function __construct(
		CrawlerServices $crawlerServices,
		SalesOfMedicine $salesOfMedicine,
		Doctor $doctor,
		Patient $patient,
		Medicine $medicine

	)
	{
		$this->crawlerServices = $crawlerServices;
		$this->salesOfMedicine = $salesOfMedicine;
		$this->doctor = $doctor;
		$this->patient = $patient;
		$this->medicine = $medicine;
	}
	public function getRecipeForCPFUser($cpf)
	{
		return $this->crawlerServices->getRecipeForCPFUser($cpf);
	}
	public function updateSalesMedicines($request)
	{
		$results = $this->crawlerServices->updateSalesMedicines($request);

		$data = [
			"user_id" => $request->user_id,
			"id_recipes_external" => $request->medicine_id,
			"title" => $request->title,
			"dosage" => $request->dosage,
			"frequency" => $request->frequency,
			"number_of_days" => $request->number_of_days,
			"validity_of_medicine" => $request->validity_of_medicine,
			"subtitle" => $request->subtitle,
			"note" => $request->note,
			"doctor_name" => $request->doctor_name,
			"doctor_cpf" => $request->doctor_cpf,
			"doctor_crm_number" => $request->doctor_crm_number,
			"patient_name" => $request->patient_name,
			"patient_cpf" => $request->patient_cpf
		];
		return	$this->storeSalesMedicines($results,$data);

	}
	public function storeSalesMedicines($retruns,$data)
	{
		if ($retruns['sales']) {
			return response()->json(['message' => $retruns['errors']]);
		}

		\DB::beginTransaction();

		try {

			$medicine = $this->medicine->create([
				'title' => $data['title'],
				'dosage' => $data['dosage'],
				'frequency' => $data['frequency'],
				'number_of_days' => $data['number_of_days'],
				'validity_of_medicine' => Util::formatStringDateToIso($data['validity_of_medicine'])->format('Y-m-d'),
				'subtitle' => $data['subtitle'],
				'note' => $data['note']
			]);

			$doctor = $this->doctor->create([
				'name' => $data['doctor_name'],
				'cpf' => $data['doctor_cpf'],
				'crm_number' => $data['doctor_crm_number'],

			]);

			$patient = $this->patient->create([
				'name' => $data['patient_name'],
				'cpf' => $data['patient_cpf']
			]);

			$this->salesOfMedicine->create([
				'user_id' => $data['user_id'],
				'id_recipes_external' => $data['id_recipes_external'],
				'doctor_id' => $doctor->id,
				'medicine_id' => $medicine->id,
				'patient_id' => $patient->id
			]);

			\DB::commit();
			return response()->json(['success' => $retruns['success']]);

		} catch (\Exception $exception){
			\DB::rollback();
			return response()->json(['error' => $exception->getMessage()]);
		}
	}
	public function listSalesMedicines($user_id)
	{
		$results = $this->salesOfMedicine->where('user_id',$user_id)->paginate(6);

		return $results->map(function ($result){
			return [
				'user' => [
					'name' => $result->user->name,
					'email' => $result->user->email,
					'cpf' => $result->user->cpf
				],
				'patient' => [
					'name' => $result->patient->name,
					'cpf' =>  $result->patient->cpf
				],
				'doctor' => [
					'name' => $result->doctor->name,
					'cpf' => $result->doctor->cpf,
					'crm_number' => $result->doctor->crm_number
				],
				'medicines' => [
					'title' => $result->medicine->title,
					'dosage' => $result->medicine->dosage,
					'frequency' => $result->medicine->frequency,
					'number_of_days' => $result->medicine->number_of_days,
					'validity_of_medicine' => $result->medicine->validity_of_medicine->format('Y/m/d'),
					'subtitle' => $result->medicine->subtitle,
					'note' => $result->medicine->note,
					'created_at' => $result->medicine->created_at->format('Y/m/d H:i:s'),
				]

			];
		});
	}
}