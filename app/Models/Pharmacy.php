<?php

namespace DrPediuPharmacies\Models;

class Pharmacy extends \DrPediuPharmacies\Models\Base\Pharmacy
{
	protected $fillable = [
		'user_id',
		'cnpj',
		'fantasy_name',
		'state_registration',
		'responsible',
		'operating_hours_of',
		'operating_hours_until'
	];
}
