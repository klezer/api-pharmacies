<?php

namespace DrPediuPharmacies\Models;

class OauthClient extends \DrPediuPharmacies\Models\Base\OauthClient
{
	protected $hidden = [
		'secret'
	];

	protected $fillable = [
		'user_id',
		'name',
		'secret',
		'redirect',
		'personal_access_client',
		'password_client',
		'revoked'
	];
}
