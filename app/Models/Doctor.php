<?php

namespace DrPediuPharmacies\Models;

class Doctor extends \DrPediuPharmacies\Models\Base\Doctor
{
	protected $fillable = [
		'name',
		'cpf',
		'crm_number'
	];
}
