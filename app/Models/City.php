<?php

namespace DrPediuPharmacies\Models;

class City extends \DrPediuPharmacies\Models\Base\City
{
	protected $fillable = [
		'state_id',
		'title'
	];
}
