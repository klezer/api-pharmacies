<?php

namespace DrPediuPharmacies\Models;

class TypesOfUser extends \DrPediuPharmacies\Models\Base\TypesOfUser
{
	protected $fillable = [
		'title'
	];
}
