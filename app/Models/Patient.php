<?php

namespace DrPediuPharmacies\Models;

class Patient extends \DrPediuPharmacies\Models\Base\Patient
{
	protected $fillable = [
		'name',
		'cpf'
	];
}
