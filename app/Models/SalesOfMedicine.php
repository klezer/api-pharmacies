<?php

namespace DrPediuPharmacies\Models;

class SalesOfMedicine extends \DrPediuPharmacies\Models\Base\SalesOfMedicine
{
	protected $fillable = [
		'user_id',
		'pharmacies_id',
		'id_recipes_external',
		'doctor_id',
		'medicine_id',
		'patient_id'
	];
}
