<?php

namespace DrPediuPharmacies\Models;

class UserAddress extends \DrPediuPharmacies\Models\Base\UserAddress
{
	protected $fillable = [
		'user_id',
		'city_id',
		'number',
		'complement',
		'street_code',
		'street_title',
		'district_title'
	];
}
