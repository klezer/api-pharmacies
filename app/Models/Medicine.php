<?php

namespace DrPediuPharmacies\Models;

class Medicine extends \DrPediuPharmacies\Models\Base\Medicine
{
	protected $fillable = [
		'title',
		'dosage',
		'frequency',
		'number_of_days',
		'validity_of_medicine',
		'subtitle',
		'note'
	];
}
