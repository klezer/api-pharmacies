<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 19:26:51 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Contact
 * 
 * @property int $id
 * @property int $user_id
 * @property string $telephone_one
 * @property string $telephone_two
 * @property string $cell_phone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediuPharmacies\Models\User $user
 *
 * @package DrPediuPharmacies\Models\Base
 */
class Contact extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\User::class);
	}
}
