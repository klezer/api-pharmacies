<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 18:43:48 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAddress
 * 
 * @property int $id
 * @property int $user_id
 * @property int $city_id
 * @property string $number
 * @property string $complement
 * @property string $street_code
 * @property string $street_title
 * @property string $district_title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediuPharmacies\Models\City $city
 * @property \DrPediuPharmacies\Models\User $user
 *
 * @package DrPediuPharmacies\Models\Base
 */
class UserAddress extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'user_address';

	protected $casts = [
		'user_id' => 'int',
		'city_id' => 'int'
	];

	public function city()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\City::class);
	}

	public function user()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\User::class);
	}
}
