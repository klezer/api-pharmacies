<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 19:26:42 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Pharmacy
 * 
 * @property int $id
 * @property int $user_id
 * @property string $cnpj
 * @property string $fantasy_name
 * @property string $state_registration
 * @property string $responsible
 * @property \Carbon\Carbon $operating_hours_of
 * @property \Carbon\Carbon $operating_hours_until
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \DrPediuPharmacies\Models\User $user
 *
 * @package DrPediuPharmacies\Models\Base
 */
class Pharmacy extends Eloquent
{
	protected $casts = [
		'user_id' => 'int'
	];

	public function user()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\User::class);
	}
}
