<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 05 Jun 2019 18:22:15 +0000.
 */

namespace DrPediuPharmacies\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $cpf
 * @property int $id_types_of_users
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \DrPediuPharmacies\Models\TypesOfUser $types_of_user
 * @property \Illuminate\Database\Eloquent\Collection $contacts
 * @property \Illuminate\Database\Eloquent\Collection $pharmacies
 * @property \Illuminate\Database\Eloquent\Collection $sales_of_medicines
 * @property \Illuminate\Database\Eloquent\Collection $user_addresses
 *
 * @package DrPediuPharmacies\Models\Base
 */
class User extends Authenticatable
{
	use Notifiable,SoftDeletes,HasApiTokens;

	protected $casts = [
		'id_types_of_users' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	public function types_of_user()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\TypesOfUser::class, 'id_types_of_users');
	}

	public function contacts()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\Contact::class);
	}

	public function pharmacies()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\Pharmacy::class);
	}

	public function sales_of_medicines()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\SalesOfMedicine::class);
	}

	public function user_addresses()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\UserAddress::class);
	}
}
