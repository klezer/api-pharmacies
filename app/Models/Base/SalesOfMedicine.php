<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 05 Jun 2019 18:22:04 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SalesOfMedicine
 * 
 * @property int $id
 * @property int $user_id
 * @property int $id_recipes_external
 * @property int $doctor_id
 * @property int $medicine_id
 * @property int $patient_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \DrPediuPharmacies\Models\Doctor $doctor
 * @property \DrPediuPharmacies\Models\Medicine $medicine
 * @property \DrPediuPharmacies\Models\Patient $patient
 * @property \DrPediuPharmacies\Models\User $user
 *
 * @package DrPediuPharmacies\Models\Base
 */
class SalesOfMedicine extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'user_id' => 'int',
		'id_recipes_external' => 'int',
		'doctor_id' => 'int',
		'medicine_id' => 'int',
		'patient_id' => 'int'
	];

	public function doctor()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\Doctor::class);
	}

	public function medicine()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\Medicine::class);
	}

	public function patient()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\Patient::class);
	}

	public function user()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\User::class);
	}
}
