<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 18:43:28 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class State
 * 
 * @property int $id
 * @property string $title
 * @property string $initials
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cities
 *
 * @package DrPediuPharmacies\Models\Base
 */
class State extends Eloquent
{
	public function cities()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\City::class);
	}
}
