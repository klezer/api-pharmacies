<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 18:43:37 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property int $state_id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \DrPediuPharmacies\Models\State $state
 * @property \Illuminate\Database\Eloquent\Collection $user_addresses
 *
 * @package DrPediuPharmacies\Models\Base
 */
class City extends Eloquent
{
	protected $table = 'citys';

	protected $casts = [
		'state_id' => 'int'
	];

	public function state()
	{
		return $this->belongsTo(\DrPediuPharmacies\Models\State::class);
	}

	public function user_addresses()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\UserAddress::class);
	}
}
