<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 05 Jun 2019 20:39:11 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OauthClient
 * 
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $secret
 * @property string $redirect
 * @property bool $personal_access_client
 * @property bool $password_client
 * @property bool $revoked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package DrPediuPharmacies\Models\Base
 */
class OauthClient extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'personal_access_client' => 'bool',
		'password_client' => 'bool',
		'revoked' => 'bool'
	];
}
