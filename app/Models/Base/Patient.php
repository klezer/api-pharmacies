<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 05 Jun 2019 18:22:31 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Patient
 * 
 * @property int $id
 * @property string $name
 * @property string $cpf
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $sales_of_medicines
 *
 * @package DrPediuPharmacies\Models\Base
 */
class Patient extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	public function sales_of_medicines()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\SalesOfMedicine::class);
	}
}
