<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Mon, 03 Jun 2019 19:26:59 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TypesOfUser
 * 
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package DrPediuPharmacies\Models\Base
 */
class TypesOfUser extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	public function users()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\User::class, 'id_types_of_users');
	}
}
