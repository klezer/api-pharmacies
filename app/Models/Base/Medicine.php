<?php

/**
 * Created by Clezer A. Ramos.
 * Date: Wed, 05 Jun 2019 18:22:49 +0000.
 */

namespace DrPediuPharmacies\Models\Base;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Medicine
 * 
 * @property int $id
 * @property string $title
 * @property string $dosage
 * @property int $frequency
 * @property int $number_of_days
 * @property \Carbon\Carbon $validity_of_medicine
 * @property string $subtitle
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $sales_of_medicines
 *
 * @package DrPediuPharmacies\Models\Base
 */
class Medicine extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'frequency' => 'int',
		'number_of_days' => 'int'
	];

	protected $dates = [
		'validity_of_medicine'
	];

	public function sales_of_medicines()
	{
		return $this->hasMany(\DrPediuPharmacies\Models\SalesOfMedicine::class);
	}
}
