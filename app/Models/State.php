<?php

namespace DrPediuPharmacies\Models;

class State extends \DrPediuPharmacies\Models\Base\State
{
	protected $fillable = [
		'title',
		'initials'
	];
}
