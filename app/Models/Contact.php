<?php

namespace DrPediuPharmacies\Models;

class Contact extends \DrPediuPharmacies\Models\Base\Contact
{
	protected $fillable = [
		'user_id',
		'telephone_one',
		'telephone_two',
		'cell_phone'
	];
}
