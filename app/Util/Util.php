<?php
/**
 * Created by PhpStorm.
 * User: Clezer A. Ramos
 * Date: 20/06/18
 * Time: 11:50
 */

namespace DrPediuPharmacies\Util;

use Carbon\Carbon;


class Util
{

    public static function formatTimeStampBr($date)
    {
        return date('d/m/Y H:i:s', strtotime(str_replace('-', '/', $date)));
    }

    public static function getDateOfBirth($date)
    {
      $dates = Carbon::parse($date);
      $data_now = Carbon::now();
      return $dates->diffInYears($data_now);
    }

    public static function formatStringDateToIso($date)
    {
        return Carbon::parse( $date);
    }

}