## Framework 

Laravel 5.8

## Dr.Pediu

Web Service para os integrar os aplicativos (Paciente e Doutor) com as Farmacias.
Com controle de vendas por Farmacia, farmacêutico, Paciente e Médico.  

## Requerimentos
- PHP 7.2 or higher
- Composer
- MySQL
- Redis
- Docker Compose

## Authors
- Clezer Ramos

## Contributing
- Clezer Ramos

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
